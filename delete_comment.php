<?php

require_once 'config/connection.php';
require_once 'classes/Comment.php';

if(isset($_GET['id'])) {
    $id = $_GET['id'];
    Comment::delete($id,$connection);
    header('Location:show_entry.php?id=' . $_GET['EntId']);
}