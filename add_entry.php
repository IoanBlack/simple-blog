<?php
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Entry</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Add Entry</h1>
                <form  action="store_entry.php" method="post">
                    <div class="form-group">
                        <div class="col-md-5">
                            <label>Title:<input type="text" class="form-control" name="title"></label>
                            <label>Intro:<textarea  cols="40" rows="5" class="form-control" name="intro"></textarea></label>
                        </div>
                        <div class="col-md-5">
                            <label>Content:<textarea cols="100" rows="10" class="form-control" name="content"></textarea></label>
                            <button class="btn btn-success form-control" type="submit">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>