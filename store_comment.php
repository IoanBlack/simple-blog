<?php

require_once 'config/connection.php';
require_once 'classes/Comment.php';

if(!empty($_POST['name']) & !empty($_POST['body'])) {

    $comment = new Comment($_POST['name'],$_POST['body'],$_POST['id']);
    $comment->store($connection);
}
header('Location:show_entry.php?id=' . $_POST['id']);
