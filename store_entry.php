<?php

require_once 'config/connection.php';
require_once 'classes/Entry.php';

if(!empty($_POST['title']) & !empty($_POST['intro']) & !empty($_POST['content'])) {

$entry = new Entry($_POST['title'],$_POST['intro'],$_POST['content']);
$entry->store($connection);
}
header('Location:index.php');