<?php

require_once 'config/connection.php';
require_once 'classes/Entry.php';

if(isset($_GET['id'])) {
    $id = $_GET['id'];
    Entry::delete($id,$connection);
    header('Location:/');
}