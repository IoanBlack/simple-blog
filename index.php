<?php

require_once 'config/connection.php';
require_once 'classes/Entry.php';

$entriesObjects = Entry::getAll($connection);

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Blog</h1>
                <a href="add_entry.php" class=" btn btn-primary">Create entry</a>
                <?php if($entriesObjects):?>
                <table class="table table-dark">
                   <thead>
                       <tr>
                           <th>Title</th>
                           <th>Intro</th>
                           <th>more</th>
                       </tr>
                   </thead>
                   <tbody>
                    <?php foreach ($entriesObjects as $entryObject):?>
                        <tr>
                            <td><?=$entryObject->getTitle()?></td>
                            <td><?=$entryObject->getIntro()?></td>
                            <td>
                                <a href="show_entry.php?id=<?=$entryObject->getId()?>" class="btn btn-info">Show</a>
                                <a href="edit_entry.php?id=<?=$entryObject->getId()?>" class="btn btn-warning">Edit</a>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <?php endif;?>
            </div>
        </div>
    </div>
</body>
</html>