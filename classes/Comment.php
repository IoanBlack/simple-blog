<?php

class Comment
{
    protected $id;
    protected $name;
    protected $body;
    protected $entryId;


    public function __construct($name, $body, $entry_id)
    {
        $this->name = $name;
        $this->body = $body;
        $this->entryId = $entry_id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getEntryId()
    {
        return $this->entryId;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    static public function getByEntryId(PDO $connection,$entryId){
        $id = htmlspecialchars($entryId);
        try {
            $sql = 'SELECT * FROM comments WHERE entry_id=:entry_id';
            $statement = $connection->prepare($sql);
            $statement->bindValue(':entry_id', $id);
            $statement->execute();
            $commentsArr = $statement->fetchAll(PDO::FETCH_ASSOC);
            $commentsObjs = [];
            foreach ($commentsArr as $commentArr) {
                $comment = new Comment($commentArr['name'],$commentArr['body'],$commentArr['entry_id']);
                $comment->setId($commentArr['id']);
                $commentsObjs[] = $comment;
                }
            return $commentsObjs;

        } catch (Exception $exception){
            echo "Error getting entry comments! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    public function store(PDO $connection){
        try {   $sql = 'INSERT INTO comments SET 
                name = :name,
                body = :body,
                entry_id = :entry_id';

            $statement = $connection->prepare($sql);
            $statement->bindValue(':name',$this->name);
            $statement->bindValue(':body',$this->body);
            $statement->bindValue(':entry_id',$this->entryId);
            $statement->execute();
        } catch(Exception $exception) {
            echo 'Error storing comment!' . $exception->getCode() . ' msg: ' . $exception->getMessage();
            die;
        }
    }

    static public function delete($id, PDO $connection){
        try{
            $sql = 'DELETE FROM comments WHERE id =:id';
            $statement = $connection->prepare($sql);
            $statement->bindValue(':id',$id);
            $statement->execute();
        } catch (Exception $exception) {
            echo 'Error deleting comment!';
            die;
        }
    }
}