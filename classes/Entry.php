<?php

class Entry
{
    protected $id;
    protected $title;
    protected $intro;
    protected $content;


    public function __construct($title,$intro,$content)
    {
        $this->title = htmlspecialchars($title);
        $this->intro = htmlspecialchars($intro);
        $this->content = htmlspecialchars($content);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getIntro()
    {
        return $this->intro;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    static public function getAll(PDO $connection){
        try {
            $sql = 'SELECT * FROM entries';
            $pdoResult = $connection->query($sql);
            $entriesArr = $pdoResult->fetchAll(PDO::FETCH_ASSOC);
            $entriesObjects = [];
            foreach ($entriesArr as $entryArr) {
               $entry = new self($entryArr['title'],$entryArr['intro'],$entryArr['content']);
                $entry->setId($entryArr['id']);
                $entriesObjects[] = $entry;}
            return $entriesObjects;
        } catch (Exception $exception){
            echo "Error getting entries! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }

    }

    static public function getById(PDO $connection,$id){
        $id = htmlspecialchars($id);
        try {
            $sql = 'SELECT * FROM entries WHERE id=:id';
            $statement = $connection->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $entriesArr = $statement->fetchAll(PDO::FETCH_ASSOC);
            $entriesArr = $entriesArr[0];
            $entry = new Entry($entriesArr['title'],$entriesArr['intro'],$entriesArr['content']);
            $entry->setId($entriesArr['id']);
            return $entry;
        } catch (Exception $exception){
            echo "Error getting entry! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    public function store(PDO $connection){


        try {
            $sql = 'INSERT INTO entries SET 
                title = :title,
                intro = :intro,
                content = :content';

            $statement = $connection->prepare($sql);
            $statement->bindValue(':title',$this->title);
            $statement->bindValue(':intro',$this->intro);
            $statement->bindValue(':content',$this->content);
            $statement->execute();
        } catch(Exception $exception) {
            echo 'Error storing entry!' . $exception->getCode() . ' msg: ' . $exception->getMessage();
            die;
        }
    }

    public function update(PDO $connection,$id){
        try {
            $sql = 'UPDATE entries SET
            title = :title,
            intro = :intro,
            content = :content
            WHERE id = :id' ;
            $statement = $connection->prepare($sql);
            $statement->execute([
                ':title' => htmlspecialchars($this->title),
                ':intro' => htmlspecialchars($this->intro),
                ':content' => htmlspecialchars($this->content),
                ':id' => htmlspecialchars($id)
            ]);

        }catch (Exception $exception){
            echo "Error updating entry! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function delete($id, PDO $connection){
        try{
            $sql = 'DELETE FROM entries WHERE id =:id';
            $statement = $connection->prepare($sql);
            $statement->bindValue(':id',$id);
            $statement->execute();
        } catch (Exception $exception) {
            echo 'Error deleting entry!';
            die;
        }
    }
}
