<?php
require_once 'connection.php';

try {
    $sql = 'CREATE TABLE entries(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    intro VARCHAR (500) NOT NULL,
    content TEXT) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
    $connection->exec($sql);

    $commentSql = 'CREATE TABLE comments(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(25) NOT NULL,
    body VARCHAR (500) NOT NULL,
    entry_id INT NOT NULL,
    FOREIGN KEY (entry_id) REFERENCES entries (id) ON DELETE CASCADE)  DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
    $connection->exec($commentSql);
    } catch(Exception $exception) {
    echo 'Error creating entries or comments table!' . $exception->getCode() . ' msg: ' . $exception->getMessage();
    die;
}
header('Location:seeder_db.php');
