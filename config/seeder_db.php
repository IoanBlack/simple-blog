<?php

require_once 'connection.php';

$entries = [
    [
        'title' => 'Great Title 1',
        'intro' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi doloremque excepturi
                    fugiat iure mollitia nemo non quae quasi qui sapiente sint tempore tenetur, unde vero?',

        'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab consequatur cum dignissimos, earum esse est incidunt, iusto
                      maiores natus numquam repudiandae sapiente voluptatem. Alias blanditiis, et minus nisi numquam officiis ratione sit
                      tenetur. Alias debitis eaque illo natus porro? Doloremque et eum illum iusto maxime officia qui quisquam vero. A atque
                      cupiditate, est eum explicabo mollitia numquam reprehenderit! Debitis ipsum iure provident, quos recusandae vitae
                      voluptatem. Ad alias delectus id magnam officia? Accusamus alias animi consequatur dignissimos dolor doloremque eius,
                      enim eos est facilis fuga ipsum iste laborum maxime molestiae molestias mollitia neque, numquam perspiciatis placeat
                      praesentium quia quibusdam rem repellat sint vero voluptas, voluptates! Debitis harum inventore nobis porro vero!
                      Asperiores aspernatur itaque nam ullam voluptatum? Ad consectetur culpa delectus doloribus ea eligendi est
                      exercitationem expedita facere id in labore laudantium nesciunt nobis odio officiis quaerat quas quia quis reiciendis
                      sequi soluta, suscipit tenetur velit veritatis voluptates voluptatibus! Earum ipsam officia optio sequi voluptatum!
                      Blanditiis commodi corporis, cum delectus dicta dolores dolorum ducimus, eius esse est eum ex facere fugit in ipsum
                      iusto maiores nisi quam quibusdam reiciendis reprehenderit, tempora voluptatum. Deleniti eligendi excepturi itaque natus
                      odio quo voluptates! Animi error labore nulla perspiciatis quis rerum unde voluptate, voluptatem! Eligendi, eos,
                      explicabo facere illum labore molestiae odio porro quas quia quod quos rem saepe sint sit tempora tenetur vel! Accusamus
                      aliquid animi aut consequuntur debitis, doloribus et facilis illum nam nostrum qui quod sit soluta ut veniam, voluptate
                      voluptatibus. Ab ad, autem dicta dolorem ipsam magni minus, molestias odit officiis porro quibusdam sed similique unde
                      ut vero. Accusamus cum, enim est eveniet facilis in nam odio, omnis rem reprehenderit, sapiente velit. Accusantium at
                      consequatur cum dolor error hic illo iure necessitatibus, nemo nostrum odit porro repellendus sed ut vitae. Dignissimo'
    ],
    [
        'title' => 'Great Title 2',
        'intro' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi doloremque excepturi
                    fugiat iure mollitia nemo non quae quasi qui sapiente sint tempore tenetur, unde vero?',

        'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab consequatur cum dignissimos, earum esse est incidunt, iusto
                      maiores natus numquam repudiandae sapiente voluptatem. Alias blanditiis, et minus nisi numquam officiis ratione sit
                      tenetur. Alias debitis eaque illo natus porro? Doloremque et eum illum iusto maxime officia qui quisquam vero. A atque
                      cupiditate, est eum explicabo mollitia numquam reprehenderit! Debitis ipsum iure provident, quos recusandae vitae
                      voluptatem. Ad alias delectus id magnam officia? Accusamus alias animi consequatur dignissimos dolor doloremque eius,
                      enim eos est facilis fuga ipsum iste laborum maxime molestiae molestias mollitia neque, numquam perspiciatis placeat
                      praesentium quia quibusdam rem repellat sint vero voluptas, voluptates! Debitis harum inventore nobis porro vero!
                      Asperiores aspernatur itaque nam ullam voluptatum? Ad consectetur culpa delectus doloribus ea eligendi est
                      exercitationem expedita facere id in labore laudantium nesciunt nobis odio officiis quaerat quas quia quis reiciendis
                      sequi soluta, suscipit tenetur velit veritatis voluptates voluptatibus! Earum ipsam officia optio sequi voluptatum!
                      Blanditiis commodi corporis, cum delectus dicta dolores dolorum ducimus, eius esse est eum ex facere fugit in ipsum
                      iusto maiores nisi quam quibusdam reiciendis reprehenderit, tempora voluptatum. Deleniti eligendi excepturi itaque natus
                      odio quo voluptates! Animi error labore nulla perspiciatis quis rerum unde voluptate, voluptatem! Eligendi, eos,
                      explicabo facere illum labore molestiae odio porro quas quia quod quos rem saepe sint sit tempora tenetur vel! Accusamus
                      aliquid animi aut consequuntur debitis, doloribus et facilis illum nam nostrum qui quod sit soluta ut veniam, voluptate
                      voluptatibus. Ab ad, autem dicta dolorem ipsam magni minus, molestias odit officiis porro quibusdam sed similique unde
                      ut vero. Accusamus cum, enim est eveniet facilis in nam odio, omnis rem reprehenderit, sapiente velit. Accusantium at
                      consequatur cum dolor error hic illo iure necessitatibus, nemo nostrum odit porro repellendus sed ut vitae. Dignissimo'
    ],
    [
        'title' => 'Great Title 3',
        'intro' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi doloremque excepturi
                    fugiat iure mollitia nemo non quae quasi qui sapiente sint tempore tenetur, unde vero?',

        'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab consequatur cum dignissimos, earum esse est incidunt, iusto
                      maiores natus numquam repudiandae sapiente voluptatem. Alias blanditiis, et minus nisi numquam officiis ratione sit
                      tenetur. Alias debitis eaque illo natus porro? Doloremque et eum illum iusto maxime officia qui quisquam vero. A atque
                      cupiditate, est eum explicabo mollitia numquam reprehenderit! Debitis ipsum iure provident, quos recusandae vitae
                      voluptatem. Ad alias delectus id magnam officia? Accusamus alias animi consequatur dignissimos dolor doloremque eius,
                      enim eos est facilis fuga ipsum iste laborum maxime molestiae molestias mollitia neque, numquam perspiciatis placeat
                      praesentium quia quibusdam rem repellat sint vero voluptas, voluptates! Debitis harum inventore nobis porro vero!
                      Asperiores aspernatur itaque nam ullam voluptatum? Ad consectetur culpa delectus doloribus ea eligendi est
                      exercitationem expedita facere id in labore laudantium nesciunt nobis odio officiis quaerat quas quia quis reiciendis
                      sequi soluta, suscipit tenetur velit veritatis voluptates voluptatibus! Earum ipsam officia optio sequi voluptatum!
                      Blanditiis commodi corporis, cum delectus dicta dolores dolorum ducimus, eius esse est eum ex facere fugit in ipsum
                      iusto maiores nisi quam quibusdam reiciendis reprehenderit, tempora voluptatum. Deleniti eligendi excepturi itaque natus
                      odio quo voluptates! Animi error labore nulla perspiciatis quis rerum unde voluptate, voluptatem! Eligendi, eos,
                      explicabo facere illum labore molestiae odio porro quas quia quod quos rem saepe sint sit tempora tenetur vel! Accusamus
                      aliquid animi aut consequuntur debitis, doloribus et facilis illum nam nostrum qui quod sit soluta ut veniam, voluptate
                      voluptatibus. Ab ad, autem dicta dolorem ipsam magni minus, molestias odit officiis porro quibusdam sed similique unde
                      ut vero. Accusamus cum, enim est eveniet facilis in nam odio, omnis rem reprehenderit, sapiente velit. Accusantium at
                      consequatur cum dolor error hic illo iure necessitatibus, nemo nostrum odit porro repellendus sed ut vitae. Dignissimo'
    ],
    [
        'title' => 'Great Title 4',
        'intro' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi doloremque excepturi
                    fugiat iure mollitia nemo non quae quasi qui sapiente sint tempore tenetur, unde vero?',

        'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab consequatur cum dignissimos, earum esse est incidunt, iusto
                      maiores natus numquam repudiandae sapiente voluptatem. Alias blanditiis, et minus nisi numquam officiis ratione sit
                      tenetur. Alias debitis eaque illo natus porro? Doloremque et eum illum iusto maxime officia qui quisquam vero. A atque
                      cupiditate, est eum explicabo mollitia numquam reprehenderit! Debitis ipsum iure provident, quos recusandae vitae
                      voluptatem. Ad alias delectus id magnam officia? Accusamus alias animi consequatur dignissimos dolor doloremque eius,
                      enim eos est facilis fuga ipsum iste laborum maxime molestiae molestias mollitia neque, numquam perspiciatis placeat
                      praesentium quia quibusdam rem repellat sint vero voluptas, voluptates! Debitis harum inventore nobis porro vero!
                      Asperiores aspernatur itaque nam ullam voluptatum? Ad consectetur culpa delectus doloribus ea eligendi est
                      exercitationem expedita facere id in labore laudantium nesciunt nobis odio officiis quaerat quas quia quis reiciendis
                      sequi soluta, suscipit tenetur velit veritatis voluptates voluptatibus! Earum ipsam officia optio sequi voluptatum!
                      Blanditiis commodi corporis, cum delectus dicta dolores dolorum ducimus, eius esse est eum ex facere fugit in ipsum
                      iusto maiores nisi quam quibusdam reiciendis reprehenderit, tempora voluptatum. Deleniti eligendi excepturi itaque natus
                      odio quo voluptates! Animi error labore nulla perspiciatis quis rerum unde voluptate, voluptatem! Eligendi, eos,
                      explicabo facere illum labore molestiae odio porro quas quia quod quos rem saepe sint sit tempora tenetur vel! Accusamus
                      aliquid animi aut consequuntur debitis, doloribus et facilis illum nam nostrum qui quod sit soluta ut veniam, voluptate
                      voluptatibus. Ab ad, autem dicta dolorem ipsam magni minus, molestias odit officiis porro quibusdam sed similique unde
                      ut vero. Accusamus cum, enim est eveniet facilis in nam odio, omnis rem reprehenderit, sapiente velit. Accusantium at
                      consequatur cum dolor error hic illo iure necessitatibus, nemo nostrum odit porro repellendus sed ut vitae. Dignissimo'
    ] ,
    [
        'title' => 'Great Title 5',
        'intro' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi doloremque excepturi
                    fugiat iure mollitia nemo non quae quasi qui sapiente sint tempore tenetur, unde vero?',

        'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab consequatur cum dignissimos, earum esse est incidunt, iusto
                      maiores natus numquam repudiandae sapiente voluptatem. Alias blanditiis, et minus nisi numquam officiis ratione sit
                      tenetur. Alias debitis eaque illo natus porro? Doloremque et eum illum iusto maxime officia qui quisquam vero. A atque
                      cupiditate, est eum explicabo mollitia numquam reprehenderit! Debitis ipsum iure provident, quos recusandae vitae
                      voluptatem. Ad alias delectus id magnam officia? Accusamus alias animi consequatur dignissimos dolor doloremque eius,
                      enim eos est facilis fuga ipsum iste laborum maxime molestiae molestias mollitia neque, numquam perspiciatis placeat
                      praesentium quia quibusdam rem repellat sint vero voluptas, voluptates! Debitis harum inventore nobis porro vero!
                      Asperiores aspernatur itaque nam ullam voluptatum? Ad consectetur culpa delectus doloribus ea eligendi est
                      exercitationem expedita facere id in labore laudantium nesciunt nobis odio officiis quaerat quas quia quis reiciendis
                      sequi soluta, suscipit tenetur velit veritatis voluptates voluptatibus! Earum ipsam officia optio sequi voluptatum!
                      Blanditiis commodi corporis, cum delectus dicta dolores dolorum ducimus, eius esse est eum ex facere fugit in ipsum
                      iusto maiores nisi quam quibusdam reiciendis reprehenderit, tempora voluptatum. Deleniti eligendi excepturi itaque natus
                      odio quo voluptates! Animi error labore nulla perspiciatis quis rerum unde voluptate, voluptatem! Eligendi, eos,
                      explicabo facere illum labore molestiae odio porro quas quia quod quos rem saepe sint sit tempora tenetur vel! Accusamus
                      aliquid animi aut consequuntur debitis, doloribus et facilis illum nam nostrum qui quod sit soluta ut veniam, voluptate
                      voluptatibus. Ab ad, autem dicta dolorem ipsam magni minus, molestias odit officiis porro quibusdam sed similique unde
                      ut vero. Accusamus cum, enim est eveniet facilis in nam odio, omnis rem reprehenderit, sapiente velit. Accusantium at
                      consequatur cum dolor error hic illo iure necessitatibus, nemo nostrum odit porro repellendus sed ut vitae. Dignissimo'
    ]
];

foreach($entries as $entry){
    try {
        $sql = 'INSERT INTO entries SET
               title = "' . $entry['title'] . '",
               intro = "' . $entry['intro'] . '",
               content = "' . $entry['content'] . '"
               ';
        $connection->exec($sql);
    } catch(Exception $exception) {
        echo 'Error inserting test data to table!' . $exception->getCode() . ' msg: ' . $exception->getMessage();
        die;
    }
}
header('Location:../index.php');