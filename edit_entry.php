<?php

require_once 'config/connection.php';
require_once 'classes/Entry.php';

if(isset($_GET['id'])) {
    $entry = Entry::getById($connection,$_GET['id'] );
}
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Entry</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Edit Entry</h1>
                <form  action="update_entry.php" method="post">
                    <div class="form-group">
                        <div class="col-md-5">
                            <input type="hidden" name="id" value="<?=$_GET['id']?>">
                            <label>Title:<input type="text" class="form-control" name="title" value="<?=$entry->getTitle()?>"></label>
                            <label>Intro:<textarea cols="40" rows="5" class="form-control" name="intro"><?=$entry->getIntro()?></textarea></label>
                        </div>
                        <div class="col-md-5">
                            <label>Content:<textarea cols="100" rows="10" class="form-control" name="content"><?=$entry->getContent()?></textarea></label>
                            <button class="btn btn-warning form-control" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
