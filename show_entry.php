<?php

require_once 'config/connection.php';
require_once 'classes/Entry.php';
require_once 'classes/Comment.php';

if(isset($_GET['id'])) {
    $id = ($_GET['id']);
}
$entry = Entry::getById($connection,$id );
$comments = Comment::getByEntryId($connection,$id);

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Details</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
                <div class="col-md-8">
                    <h1><?=$entry->getTitle()?></h1>
                    <div>
                        <p> <?=$entry->getContent()?></p>
                    </div>
                    <a onclick="return confirm('are you sure?')" href="delete_entry.php?id=<?=$id?>" class="btn btn-danger">Delete</a>
                    <a href="/" class="btn btn-primary">Back</a>
                </div>
            <?php if($comments):?>
                <div class="col-md-4">
                    <h4>Comments</h4>
                     <?php foreach($comments as $comment):?>

                    <table class="table">
                        <thead>
                            <tr>
                                <th><?=$comment->getName()?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?=$comment->getBody()?></td>
                                <td> <a onclick="return confirm('are you sure?')" href="delete_comment.php?id=<?=$comment->getId()?>&EntId=<?=$id?>" class="btn btn-danger">Delete</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <?php endforeach; endif; ?>
                </div>
                <div class="col-md-5">
                    <form action="store_comment.php" method="post">
                        <div class="form-group">
                            <input type="hidden" name="id" value="<?=$id?>">
                            <label>Name:<input type="text"  name="name" class="form-control""></label>
                            <label>Comment:<textarea  cols="40" rows="5" name="body" class="form-control"></textarea></label>
                            <button type="submit"  class="form-control btn btn-success" >Leave Comment</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</body>
</html>
